{# UTM TRACKING #}
{% set utm_campaign = "enewsletter" %}
{% set utm_source = "prospect-welcome-car-enewsletter-oct-2019" %} {# UTM Builder #}
{% set utm_medium = "email" %}


{# HEADER #}
{% include "partials/head.nunjucks" %}


{# PREHEADER #}
{% set preheader = "October 2019 Newsletter" %}


{# LEAD ARTICLE #}
{% set greeting     = "Hello %%First_name%%," %}
{% set email_intro  = "We are getting in touch with our latest news, as you previously requested information about the Motability Scheme. Highlights this month include the latest prices for the Motability Scheme, Helen Dolphin interviewing Scheme experts and the latest car safety technology." %}



{# LEAD ARTICLE - 12#}
{% set lead_article_cate_type   = "blue" %} {# blue #}
{% set image_name               = "latest-prices" %}
{% set image_url                = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/e869e1cd-bf75-4063-8aae-e2540aad50d7.jpg" %} {# Image size, width: 580px | height: 218px #}
{% set headline                 = "Latest prices for the Motability Scheme" %}
{% set body_copy                = "Our range of over 2,000 vehicles available through the Motability Scheme has now been updated. We offer you a choice of vehicles that are both affordable and suitable for your needs, and with such a wide variety available, we’re sure you’ll be able to find one that’s right for you." %}
{% set cta_button_copy          = "See latest prices" %}
{% set link_image               = "https://news.motability.co.uk/scheme-news/latest-prices-for-the-motability-scheme/" %}  
{% set link_headline            = "" %}
{% set link_cta                 = "" %}
{% include "partials/header.nunjucks" %}


{# STORIES 1 & 2 - 6 11#}
{% set article_cate_type_1  = "blue" %} {# blue #}
{% set image_name_1         = "decide" %}
{% set image_url_1          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/5f385805-a91e-4bc7-af4d-cfca151c80b5.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_1           = "10 things to help you <br class='mobile-hide'>decide if the Scheme is for you" %}
{% set body_copy_1          = "We've highlighted a few things that people often want to talk to us about before deciding if the Scheme is for them." %}
{% set cta_button_copy_1    = "Find out more" %}
{% set link_image_1         = "https://news.motability.co.uk/scheme-news/10-things-to-help-you-decide-if-the-motability-scheme-is-right-for-you/" %}  
{% set link_headline_1      = "" %}
{% set link_cta_1           = "" %}

{% set article_cate_type_2  = "blue" %} {# blue #}
{% set image_name_2         = "chooseoptions" %}
{% set image_url_2          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/7f35e7a2-3b4a-429d-84f1-079aa312e3f1.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_2           = "Choose from 4 <br class='mobile-hide'>options" %}
{% set body_copy_2          = "Choose from a huge range of cars, scooters, powered wheelchairs or Wheelchair Accessible Vehicles, all of which come with our fully inclusive lease package." %}
{% set cta_button_copy_2    = "Find out more" %}
{% set link_image_2         = "https://news.motability.co.uk/scheme-news/choose-from-4-options-heres-whats-available-through-the-motability-scheme/" %}  
{% set link_headline_2      = "" %}
{% set link_cta_2           = "" %}

{% include "snippets/two-stories.nunjucks" %}
    

{# STORIES 3 & 4 - 3 1#}
{% set article_cate_type_1  = "blue" %} {# blue #}
{% set image_name_1         = "Barbara" %}
{% set image_url_1          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/07787fd6-ec64-4c24-8e08-75823ab9f9a5.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_1           = "Barbara’s story" %}
{% set body_copy_1          = "‘There’s no way I’d be able to do the course without my car… it’s like a member of my family.’ In this video, Barbara explains how the Motability Scheme has reinstated her independence, allowing her to pursue her career goals." %}
{% set cta_button_copy_1    = "Watch video" %}
{% set link_image_1         = "https://news.motability.co.uk/inspiration/how-the-scheme-has-made-a-difference-to-barbaras-life/" %}  
{% set link_headline_1      = "" %}
{% set link_cta_1           = "" %}

{% set article_cate_type_2  = "blue" %} {# blue #}
{% set image_name_2         = "Helenaskshani" %}
{% set image_url_2          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/4cc58e75-eea8-40c1-b275-e4794514cc3e.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_2           = "Helen Asks" %}
{% set body_copy_2          = "Thank you to everyone who sent in questions for Helen, the response has been overwhelming. In this video, Helen asks Motability Scheme expert Hani to answer some of the most commonly asked questions from customers." %}
{% set cta_button_copy_2    = "See answers" %}
{% set link_image_2         = "https://www.motability.co.uk/c/ask-helen/" %}  
{% set link_headline_2      = "" %}
{% set link_cta_2           = "" %}

{% include "snippets/two-stories.nunjucks" %}


{# STORIES 5 & 6 - 8 7#}
{% set article_cate_type_1  = "blue" %} {# blue #}
{% set image_name_1         = "safety" %}
{% set image_url_1          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/3674cfce-0c90-4c86-8a87-9cfe491014c7.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_1           = "The latest car safety <br class='mobile-hide'>technology" %}
{% set body_copy_1          = "Automotive safety has advanced enormously in recent times. Technology is behind much of the developments, and new car safety features are being rolled out rapidly. Here we list some of the latest tech." %}
{% set cta_button_copy_1    = "Find out more" %}
{% set link_image_1         = "https://news.motability.co.uk/motoring/the-latest-car-safety-technology-for-october-2019/" %}  
{% set link_headline_1      = "" %}
{% set link_cta_1           = "" %}

{% set article_cate_type_2  = "blue" %} {# blue #}
{% set image_name_2         = "Intergenerational" %}
{% set image_url_2          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/2c3c541a-f77d-464c-901b-f17d460358b6.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_2           = "Intergenerational days <br class='mobile-hide'>out in the UK" %}
{% set body_copy_2          = "Planning a day trip with the entire family? In this article Rough Guides pick seven of the country’s top accessible attractions that will keep every generation happy.<br>&nbsp;" %}
{% set cta_button_copy_2    = "View article" %}
{% set link_image_2         = "https://news.motability.co.uk/places/accessible-intergenerational-days-out-around-the-uk/" %}  
{% set link_headline_2      = "" %}
{% set link_cta_2           = "" %}

{% include "snippets/two-stories.nunjucks" %}


{# STORY 6 - 10#}

{% set article_cate_type  = "blue" %} {# blue #}
{% set image_name         = "electric" %}
{% set image_url          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/364628d7-e24e-4e8d-a90e-4c4e9d5c8882.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline           = "Electric vehicles explained" %}
{% set body_copy          = "Electric vehicles are becoming increasing common and their popularity is expected to rise in the near future. This article provides insight into electric cars and explains the jargon that surrounds them." %}
{% set cta_button_copy    = "Learn more" %}
{% set link_image         = "https://news.motability.co.uk/motoring/electric-cars-how-to-speak-the-language/" %}  
{% set link_headline      = "" %}
{% set link_cta           = "" %}

{% include "snippets/fw-story.nunjucks" %}

{# News Links #}

{% set article_cate_type  = "blue" %}

{% set cta_button_copy_1    = "Everything you need to know about named drivers on the Motability Scheme" %}
{% set link_cta_1           = "https://news.motability.co.uk/scheme-news/everything-you-need-to-know-about-named-drivers-on-the-motability-scheme/" %}

{% set cta_button_copy_2    = "Steering adaptations explained" %}
{% set link_cta_2           = "https://news.motability.co.uk/motoring/steering-adaptations-explained/" %}

{% set cta_button_copy_3    = "Putting a face to a name" %}
{% set link_cta_3           = "https://news.motability.co.uk/scheme-news/putting-a-face-to-a-name/" %}

{% set cta_button_copy_4    = "" %}
{% set link_cta_4           = "" %}

{% set cta_button_copy_5   = "" %}
{% set link_cta_5           = "" %}
{% include "snippets/fw-news-links.nunjucks" %}


{# FOOTER #}
{% set footer_type = "prospects" %} {# sign-up | prospects | customer | customer-pws #}
{% include "partials/footer-new.nunjucks" %}