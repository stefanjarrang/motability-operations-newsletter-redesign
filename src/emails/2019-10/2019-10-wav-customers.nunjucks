{# UTM TRACKING #}
{% set utm_campaign = "enewsletter" %}
{% set utm_source = "wav-enewsletter-oct-2019" %} {# UTM Builder #}
{% set utm_medium = "email" %}


{# HEADER #}
{% include "partials/head.nunjucks" %}


{# PREHEADER #}
{% set preheader = "October 2019 Newsletter" %}


{# LEAD ARTICLE #}
{% set greeting     = "Hello %%First_name%%," %}
{% set email_intro  = "Welcome to your Motability Scheme e-newsletter. Highlights this month include top tips for your tie-downs, Helen Dolphin interviewing Scheme experts and intergenerational days out." %}



{# LEAD ARTICLE - 13 #}
{% set lead_article_cate_type   = "blue" %} {# blue #}
{% set image_name               = "tiedowns" %}
{% set image_url                = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/8ee6c507-e1d5-4db5-8b97-6683806734a5.jpg" %} {# Image size, width: 580px | height: 218px #}
{% set headline                 = "Top tips for using your tie-downs" %}
{% set body_copy                = "Tie-downs, or restraints, are necessary in securing the wheelchair-user in the WAV, so it’s important to keep them in good working order. Watch our video to see Brian from Alfred Bekker explain how to use your WAV’s tie-downs correctly." %}
{% set cta_button_copy          = "Watch clip" %}
{% set link_image               = "https://news.motability.co.uk/everyday-tips/how-to-use-your-wavs-tie-downs/" %}  
{% set link_headline            = "" %}
{% set link_cta                 = "" %}
{% include "partials/header.nunjucks" %}


{# STORIES 1 & 2 - 1 3 #}
{% set article_cate_type_1  = "blue" %} {# blue #}
{% set image_name_1         = "Helenaskshani" %}
{% set image_url_1          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/4cc58e75-eea8-40c1-b275-e4794514cc3e.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_1           = "Helen Asks" %}
{% set body_copy_1          = "Thank you to everyone who sent in questions for Helen, the response has been overwhelming. In this video, Helen asks Motability Scheme expert Hani to answer some of the most commonly asked questions from customers." %}
{% set cta_button_copy_1    = "See answers" %}
{% set link_image_1         = "https://www.motability.co.uk/c/ask-helen/" %}  
{% set link_headline_1      = "" %}
{% set link_cta_1           = "" %}

{% set article_cate_type_2  = "blue" %} {# blue #}
{% set image_name_2         = "Barbara" %}
{% set image_url_2          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/07787fd6-ec64-4c24-8e08-75823ab9f9a5.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_2           = "Barbara’s story" %}
{% set body_copy_2          = "‘There’s no way I’d be able to do the course without my car… it’s like a member of my family.’ In this video, Barbara explains how the Motability Scheme has reinstated her independence, allowing her to pursue her career goals." %}
{% set cta_button_copy_2    = "Watch video" %}
{% set link_image_2         = "https://news.motability.co.uk/inspiration/how-the-scheme-has-made-a-difference-to-barbaras-life/" %}  
{% set link_headline_2      = "" %}
{% set link_cta_2           = "" %}

{% include "snippets/two-stories.nunjucks" %}
    

{# STORIES 3 & 4 - 8 7#}
{% set article_cate_type_1  = "blue" %} {# blue #}
{% set image_name_1         = "safety" %}
{% set image_url_1          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/3674cfce-0c90-4c86-8a87-9cfe491014c7.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_1           = "The latest car safety <br class='mobile-hide'>technology" %}
{% set body_copy_1          = "Automotive safety has advanced enormously in recent times. Technology is behind much of the developments, and new car safety features are being rolled out rapidly. Here we list some of the latest tech." %}
{% set cta_button_copy_1    = "Find out more" %}
{% set link_image_1         = "https://news.motability.co.uk/motoring/the-latest-car-safety-technology-for-october-2019/" %}  
{% set link_headline_1      = "" %}
{% set link_cta_1           = "" %}

{% set article_cate_type_2  = "blue" %} {# blue #}
{% set image_name_2         = "Intergenerational" %}
{% set image_url_2          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/2c3c541a-f77d-464c-901b-f17d460358b6.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_2           = "Intergenerational days <br class='mobile-hide'>out in the UK" %}
{% set body_copy_2          = "Planning a day trip with the entire family? In this article Rough Guides pick seven of the country’s top accessible attractions that will keep every generation happy.<br>&nbsp;" %}
{% set cta_button_copy_2    = "View article" %}
{% set link_image_2         = "https://news.motability.co.uk/places/accessible-intergenerational-days-out-around-the-uk/" %}  
{% set link_headline_2      = "" %}
{% set link_cta_2           = "" %}

{% include "snippets/two-stories.nunjucks" %}


{# STORIES 5 & 6 - 9 10 #}
{% set article_cate_type_1  = "blue" %} {# blue #}
{% set image_name_1         = "selfcare" %}
{% set image_url_1          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/dfc56bb6-64f7-41a1-8d7c-c8b9641cdff7.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_1           = "The importance of <br class='mobile-hide'>self-care" %}
{% set body_copy_1          = "With the importance of mental health becoming less of a taboo, more and more people are prioritising self-care. Blogger and Motability Scheme customer Sarah considers the importance of self-care and explains how she practices it." %}
{% set cta_button_copy_1    = "Read more" %}
{% set link_image_1         = "https://news.motability.co.uk/everyday-tips/the-importance-of-self-care-and-how-i-practice-it/" %}  
{% set link_headline_1      = "" %}
{% set link_cta_1           = "" %}

{% set article_cate_type_2  = "blue" %} {# blue #}
{% set image_name_2         = "electric" %}
{% set image_url_2          = "http://image.email.motabilityoperations.co.uk/lib/fe8712717663037a7c/m/3/14ba8d62-355c-410e-81f7-3aec3acb1494.jpg" %} {# Image size, width: 450px | height: 280px #}
{% set headline_2           = "Electric vehicles <br class='mobile-hide'>explained" %}
{% set body_copy_2          = "Electric vehicles are becoming increasing common and their popularity is expected to rise in the near future. This article provides insight into electric cars and explains the jargon that surrounds them.<br>&nbsp;" %}
{% set cta_button_copy_2    = "Learn more" %}
{% set link_image_2         = "https://news.motability.co.uk/motoring/electric-cars-how-to-speak-the-language/" %}  
{% set link_headline_2      = "" %}
{% set link_cta_2           = "" %}

{% include "snippets/two-stories.nunjucks" %}

{# News Links #}

{% set article_cate_type  = "blue" %}

{% set cta_button_copy_1    = "How to access disabled parking concessions across the UK" %}
{% set link_cta_1           = "https://news.motability.co.uk/everyday-tips/how-to-access-disabled-parking-concessions-across-the-uk/" %}

{% set cta_button_copy_2    = "Steering adaptations explained" %}
{% set link_cta_2           = "https://news.motability.co.uk/motoring/steering-adaptations-explained/" %}

{% set cta_button_copy_3    = "Putting a face to a name" %}
{% set link_cta_3           = "https://news.motability.co.uk/scheme-news/putting-a-face-to-a-name/" %}

{% set cta_button_copy_4    = "Article 4" %}
{% set link_cta_4           = "" %}

{% set cta_button_copy_5   = "Article 5" %}
{% set link_cta_5           = "" %}
{% include "snippets/fw-news-links.nunjucks" %}


{# FOOTER #}
{% set footer_type = "customer-wav" %} {# sign-up | prospects | customer | customer-pws #}
{% include "partials/footer-new.nunjucks" %}